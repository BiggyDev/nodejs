var serverHttp = require('http');
var url = require('url');

var serveur = serverHttp.createServer(function (request, response) {
    var rubrique = url.parse(request.url).pathname;
    response.writeHead(200, {"Content-Type": "text/html"});
    if (rubrique == "/") {
        response.write("<h1>Acceuil</h1>")
    } else if (rubrique == "/michel") {
        response.write("<h1>La page de Michel</h1>")
    }
    response.end();
});

serveur.listen(9999);